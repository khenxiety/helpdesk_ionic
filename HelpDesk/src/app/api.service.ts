import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ApiService {
  headers: HttpHeaders;
  constructor(
    public http: HttpClient
  ) {
    this.headers = new HttpHeaders();
    this.headers.append("Accept", 'application/json');
    this.headers.append("Content-Type", 'application/json');
    this.headers.append("Access-Control-Allow-Origin", '*');


   }

  // addStudent(data){
  //   return this.http.post('http://localhost/Projects/fullstack/server/create.php', data);
  // }

  getAccounts(id){
    return this.http.get('http://localhost/Projects/helpdesk_ionic/backend/account.php?id='+id);
  }


  getTickets(id){
    return this.http.get('http://localhost/Projects/helpdesk_ionic/backend/getsingleData.php?id='+id);
  }
  getsingleTickets(id){
    return this.http.get('http://localhost/Projects/helpdesk_ionic/backend/getsingletickets.php?id='+id);
  }


  submitTicket(d){
    return this.http.post('http://localhost/Projects/helpdesk_ionic/backend/add-tickets.php', d);

  }


  delete(id){
    return this.http.delete('http://localhost/Projects/helpdesk_ionic/backend/deletestudent.php?id='+id);


  }



  getMessages(){
    return this.http.get('http://localhost/Projects/helpdesk_ionic/backend/getmessages.php');
  }

  sendMessage(data){
    return this.http.post('http://localhost/Projects/helpdesk_ionic/backend/sendMessage.php',data);
  }


  getSchedule(id){
    return this.http.get('http://localhost/Projects/helpdesk_ionic/backend/getschedule.php?id='+id);

  }



  updateTicket(id,data){
    return this.http.put('http://localhost/Projects/helpdesk_ionic/backend/updateStudent.php?id='+id,data);

  }
}
