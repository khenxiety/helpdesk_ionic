import { Component, OnInit } from '@angular/core';
import { NavigationExtras, Router } from '@angular/router';
import { ToastController } from '@ionic/angular';
import { ApiService } from 'src/app/api.service';
import { NavparamsService } from 'src/app/services/navparams.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  sr_code: any ;
  password: any;

  accounts:any[];
  username: any=[];
  pass: any=[];

  id:any=[];

  account:any[];
 sr = "18-52647";
  constructor(
    public navparams:NavparamsService,
    private router: Router,
    public toastController:ToastController,
    public apiservice:ApiService
  ) { 
    // delete this
    this.getAccounts()
    
  }

  ngOnInit() {
  }
  // reads data from api

  async getAccounts(){

    
    this.apiservice.getAccounts(this.sr_code).subscribe((res:any)=> {
      console.log("Success===",res);
      this.accounts=res;
      
      
      // getting objects from an array

      // this.accounts.forEach(c => {
      //   this.username.push(c.sr_code);
      //   this.pass.push(c.password);
      //   this.id.push(c.id);
      // });
     

    },(error)=> {
      console.log("Error===",error);
    })
    
    

    
    


    

    
      // validation code
    

    
    

  }

  async check(){
    const failed = await this.toastController.create({
          message: 'Login Failed, Invalid account.',
          duration: 2000
        });

    const success = await this.toastController.create({
        message: 'Login Success.',
          duration: 2000
      });
        

    





    this.accounts.forEach(c => {

      if(this.sr_code == c.sr_code && this.password==c.password){


        
        this.navparams.setUser(c.id)
        this.router.navigate(['/homepage']);
        success.present();
       
        
      }else{
        failed.present();

      }
      
    });

    // this.apiservice.getAccounts(this.sr_code).subscribe((res:any)=> {
    //   console.log("Success===",res);
    //   this.accounts=res;
    //   console.log(res);
    //   res.forEach(c => {
    //     this.username.push(c.sr_code);
    //     this.pass.push(c.password);
    //     this.id.push(c.id);
        
    //   });
      
    //   // getting objects from an array

    //   // this.accounts.forEach(c => {
    //   //   this.username.push(c.sr_code);
    //   //   this.pass.push(c.password);
    //   //   this.id.push(c.id);
    //   // });
     

    // },(error)=> {
    //   console.log("Error===",error);
    // })
    // if (this.username.includes(this.sr_code) && this.pass.includes(this.password) ){
      
    //   let navigationExtras:NavigationExtras = {
    //     queryParams:{
    //       special:this.id
    //     }
      
    //   }
    //   this.router.navigate(['/homepage'], navigationExtras);
    //   console.log(this.accounts);

    // }else{
    //   console.log("heck");
    //   const toast = await this.toastController.create({
    //     message: 'Login Failed, Invalid account.',
    //     duration: 2000
    //   });
    //   toast.present();
    // }
    // console.log(this.accounts);
  }
  
  
  

  

}
