import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastController } from '@ionic/angular';
import { ApiService } from 'src/app/api.service';
import { NavparamsService } from 'src/app/services/navparams.service';

@Component({
  selector: 'app-add-ticket',
  templateUrl: './add-ticket.page.html',
  styleUrls: ['./add-ticket.page.scss'],
})
export class AddTicketPage implements OnInit {

  user_id:any=[];

  id:any;

  
  title:any;
  priority:any;
  description:any;
  status:any;
  category:any;
  

  constructor(
    public navparams:NavparamsService,
    public route: ActivatedRoute,
    private router:Router,
    public toastController: ToastController,
    public apiservice:ApiService
  ) { 
    this.id=this.navparams.getUser();



    
  }

  ngOnInit() {
  }

  

  async submitTicket(){
    

    
    
    





    if(this.title==null){
      const toast = await this.toastController.create({
        message: 'Please put a title',
        duration: 2000
      });
      toast.present();
    }else if(this.category==undefined){
      const toast = await this.toastController.create({
        message: 'Please select a category',
        duration: 2000
      });
      toast.present();

    }
    
    else if(this.priority==null){
      const toast = await this.toastController.create({
        message: 'Please put a priority',
        duration: 2000
      });
      toast.present();
    }else if(this.description==null){
      const toast = await this.toastController.create({
        message: 'Please put a description',
        duration: 2000
      });
      toast.present();

    }else{
      let d ={
        user_id:this.id,
        title:this.title,
        priority:this.priority,
        description:this.description,
        status:this.status
      }
      
  
      this.apiservice.submitTicket(d).subscribe((res:any)=> {
        console.log("Success===",res);
  
        
      },(error)=> {
        console.log("Error===",error);
      })
    
      this.router.navigate(['/homepage']);


      const toast = await this.toastController.create({
        message: 'Ticket Added',
        duration: 2000
      });
      toast.present();

      
     
      this.router.navigate(['/homepage']);
    }

    
  }

}
