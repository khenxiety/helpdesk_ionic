import { Component, OnInit, ViewChild } from '@angular/core';
import { IonContent } from '@ionic/angular';
import { ApiService } from 'src/app/api.service';

@Component({
  selector: 'app-messages',
  templateUrl: './messages.page.html',
  styleUrls: ['./messages.page.scss'],
})
export class MessagesPage implements OnInit {


  messages:any;


  schedule:any;

  user_id:any;
  // messages = [
  //   {
  //     user:'BSU Staff',
  //     createdAt:1554090856000,
  //     message:'Hi'

  //   },
  //   {
  //     user:'18-52647',
  //     createdAt:1554090856000,
  //     message:'Hi'

  //   }


  // ]

  message:any;

  current_user="1";



  @ViewChild(IonContent) content: IonContent;

  constructor(public api_service: ApiService) { 
    this.getMessages();
    this.getSchedule();

  }


  ngOnInit() {
  }

  getMessages(){
    this.api_service.getMessages().subscribe((res:any)=> {
      console.log("Success===",res);
      this.messages=res;
      
      
      

    },(error)=> {
      console.log("Error===",error);
    })


    console.log(this.messages);
  }


  getSchedule(){
    this.api_service.getSchedule(this.current_user).subscribe((res:any)=> {
      console.log("Success===",res);
      this.schedule=res;
      
      
      

    },(error)=> {
      console.log("Error===",error);
    })


    console.log(this.schedule);
  }


  sendMessage(){

    
    let data ={
      user_id:this.current_user,
      message:this.message
      
    }
    

    this.api_service.sendMessage(data).subscribe((res:any)=> {
      console.log("Success===",res);

      this.getMessages();
    },(error)=> {
      console.log("Error===",error);
    })
    
    this.message="";
    setTimeout(() => {
      this.content.scrollToBottom(200);
    })
    
  }

}
