import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { MainpagePage } from './mainpage.page';

const routes: Routes = [
  {
    path: 'tabs',
    component: MainpagePage,
    children:[
      {
        path:'homepage',
        children:[
          {
            path:'',
            loadChildren:()=> import('../homepage/homepage.module').then(m => m.HomepagePageModule)
          }
        ]
      },
      {
        path:'messages',
        children:[
          {
            path:'',
            loadChildren:()=> import('../messages/messages.module').then(m => m.MessagesPageModule)
          }
        ]
      },
      {
        path:'',
        redirectTo:'/tabs/homepage',
        pathMatch:'full'
      }
    ]
  },
  {
    path:'',
    redirectTo:'/tabs/homepage',
    pathMatch:'full'
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class MainpagePageRoutingModule {}
