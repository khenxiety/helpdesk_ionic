import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastController } from '@ionic/angular';
import { ApiService } from 'src/app/api.service';
import { NavparamsService } from 'src/app/services/navparams.service';

@Component({
  selector: 'app-update',
  templateUrl: './update.page.html',
  styleUrls: ['./update.page.scss'],
})
export class UpdatePage implements OnInit {


  id:any;

  user_id:any;




  title:any;
  category:any;
  priority:any;
  description:any;
  status:any;

  constructor(public router:Router ,public toastcontroller: ToastController,public navparams: NavparamsService,public route: ActivatedRoute,public apiservice:ApiService) { 
    this.id=this.route.snapshot.paramMap.get('id');

    this.user_id=this.navparams.getUser();

    this.getTickets();
  }

  ngOnInit() {
  }

  getTickets(){
    this.apiservice.getsingleTickets(this.id).subscribe((res:any)=> {
      console.log("Success===",res);
      
      res.forEach(c => {
        this.title=c.title
        this.category=c.category
        this.priority=c.priority
        this.description=c.description
        this.status=c.status
        
      });
      
      

    },(error)=> {
      console.log("Error===",error);
    })


    console.log(this.id);
  }


  async update(){
    
    let d ={
      id:this.id,
      user_id:this.user_id,
      title:this.title,
      priority:this.priority,
      description:this.description,
      status:this.status
    }
    

    this.apiservice.updateTicket(this.id,d).subscribe((res:any)=> {
      console.log("Success===",res);

      
    },(error)=> {
      console.log("Error===",error);
    })
    const success = await this.toastcontroller.create({
      message: 'Update Success.',
        duration: 2000
    });

    
    
    success.present();

    this.router.navigate(['homepage']);

  
    

  }

}
