import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, NavigationExtras, Router } from '@angular/router';
import { ApiService } from 'src/app/api.service';
import { NavparamsService } from 'src/app/services/navparams.service';

@Component({
  selector: 'app-homepage',
  templateUrl: './homepage.page.html',
  styleUrls: ['./homepage.page.scss'],
})
export class HomepagePage implements OnInit {


  id:any;

  tickets:any=[];

  userID:any=[];

  constructor(private route:ActivatedRoute, 
    private router:Router,
    public api_service:ApiService,
    public navparams: NavparamsService
    ) {

      this.id=this.navparams.getUser();
      
      
      this.getTickets();
    }

  ngOnInit() {
  }

  getTickets(){

    

    this.api_service.getTickets(this.id).subscribe((res:any)=> {
      console.log("Success===",res);
      this.tickets=res;
      
      this.tickets.forEach(c => {
        this.userID.push(c.user_id)
      });
      

    },(error)=> {
      console.log("Error===",error);
    })


    console.log(this.id);
  }


  addticket(){

    

    this.router.navigate(['/add-ticket']);
  }
  
  delete(id){
    this.api_service.delete(id).subscribe((res:any)=> {
      console.log("Success===",res);
      this.getTickets();
      

    },(error:any)=> {
      console.log("Error===",error);
    })
  }


  update(id){
    this.router.navigate(['/update',id]);
  }

}
